unit Bazinis;

interface

uses
  SysUtils;

type DKunas = record
  pavadinimas: String;
  mase: Real;
  ar_gyvenama: Boolean;
  id: Integer;
  end;

BazinisTipas = DKunas;

procedure Isvesti(x: BazinisTipas);

implementation

procedure Isvesti(x: BazinisTipas);
begin
    Write(x.pavadinimas, ' ', x.mase:0:2, ' ');
    if x.ar_gyvenama then
       Write('t ')
    else
      Write('n ');
    WriteLn(x.id);
end;


end.

