unit Sarasas;

{$mode objfpc}{$H+}

interface

uses
  Bazinis;

type
  BazineNuoroda = ^BazinisTipas;

type
  ElementoNuoroda = ^Elementas;

  Elementas = record
    reiksme: BazinisTipas;
    kitas: ElementoNuoroda;
  end;

type
  SarasoT = record
    galva: ElementoNuoroda;
  end;

procedure Initialize(var L: SarasoT);
procedure Insert(var L: SarasoT; x: BazinisTipas; i: integer);
procedure Delete(var L: SarasoT; i: integer);
function Retrieve(var L: SarasoT; i: integer): BazineNuoroda;
procedure Kill(var L: SarasoT);
procedure Print(var L: SarasoT);

implementation

procedure Initialize(var L: SarasoT);
begin
  L.galva := nil;
end;

procedure Insert(var L: SarasoT; x: BazinisTipas; i: integer);
var
  k: integer;
  pries, po: ElementoNuoroda;
begin
  if i < 1 then
     exit;
  pries := L.galva;
  if pries <> nil then
  begin
    k := 1;
    while k < i do
    begin
      if pries^.kitas = nil then
        break;
      pries := pries^.kitas;
    end;
    if pries = nil then
    begin
      pries := new(ElementoNuoroda);
      pries^.reiksme := x;
      pries^.kitas := nil;
    end
    else
    begin
      po := pries^.kitas;
      pries^.kitas := new(ElementoNuoroda);
      pries^.kitas^.reiksme := x;
      pries^.kitas^.kitas := po;
    end;
  end
  else
  begin
    L.galva := new(ElementoNuoroda);
    L.galva^.reiksme := x;
    L.galva^.kitas := nil;
  end;
end;

procedure Delete(var L: SarasoT; i: integer);
var
  pries, po: ElementoNuoroda;
  k: integer;
begin
  if i < 1 then
     exit;
  pries := L.galva;
  if pries <> nil then
  begin
    if i = 1 then
    begin
      L.galva := pries^.kitas;
      Dispose(pries);
    end
    else
    begin
      k := 2;
      while k < i do
      begin
        if pries^.kitas = nil then
          break;
        pries := pries^.kitas;
        k := k + 1;
      end;
      if k = i then
      begin
        if pries^.kitas <> nil then
        begin
          po := pries^.kitas^.kitas;
          dispose(pries^.kitas);
          pries^.kitas := po;
        end;
      end;
    end;
  end;
end;

function Retrieve(var L: SarasoT; i: integer): BazineNuoroda;
var
  k: integer;
  p: ElementoNuoroda;
begin
  if i < 1 then
     exit;
  p := L.galva;
  k := 1;
  while k<i do
  begin
    if p = nil then
       break;
    p := p^.kitas;
    k := k + 1;
  end;
  if (k=i) and (p<>nil) then
     Result := @(p^.reiksme)
  else
    Result := nil;
end;

procedure Print(var L: SarasoT);
var p: ElementoNuoroda;
  i: Integer;
begin
    p := L.galva;
    i := 1;
    while p <> nil do
    begin
      Write(i, '. ');
      Isvesti(p^.reiksme);
      p := p^.kitas;
      i := i + 1;
    end;
end;

procedure Kill(var L: SarasoT);
var p, po: ElementoNuoroda;
begin
    p := L.galva;
    while p <> nil do
    begin
      po := p^.kitas;
      Dispose(p);
      p := po;
    end;
    L.galva:=nil;
end;

end.
