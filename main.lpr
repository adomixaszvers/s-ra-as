program main;

uses Bazinis, Sarasas, Sysutils;

procedure IsFailo(var L: SarasoT);
var x: BazinisTipas;
    ch: char;
    df: Text;
    k: integer;
begin
  Assign(df, 'duom.txt');
  try
      Kill(L);
     Initialize(L);
     Reset(df);
  try
     k := 1;
     while not Eof(df) do
     begin
         ReadLn(df, x.pavadinimas);
         ReadLn(df, x.mase);
         ReadLn(df, ch);
         x.ar_gyvenama:= ch = 't';
         ReadLn(df, x.id);
         Insert(L, x, k);
         ReadLn(df);
         k := k + 1;
     end;
  finally
    Close(df);
  end;
  except
  end;
end;

var sukurta: Boolean;
    pasirinkimas, ch: char;
    x: BazinisTipas;
    i: Integer;
    L: SarasoT;
    p: BazineNuoroda;
begin
  sukurta := False;

  while True do
  begin
      WriteLn('==================================');
      if not sukurta then
      begin
        WriteLn('1. Initialize(L)');
        WriteLn('2. Is failo');
        WriteLn('Bet kas kitas isjungs programa');
        Write('Jusu pasirinkimas: ');
        ReadLn(pasirinkimas);
        case pasirinkimas of
        '1':
          begin
              Initialize(L);
              sukurta := True;
          end;
        '2':
          begin
              IsFailo(L);
              sukurta := True;
          end;
        else
          Exit;
        end;
      end
      else
      begin
          WriteLn('1. Insert(L, x, i)');
          WriteLn('2. Delete(L, i)');
          WriteLn('3. Retrieve(L, i)');
          WriteLn('4. Kill(L)');
          WriteLn('5. Print(L)');
          WriteLn('Bet kas kitas uzbaigs programa');
          Write('Jusu pasirinkimas: ');
          ReadLn(pasirinkimas);
          case pasirinkimas of
          '1':
            begin
                try
                    Write('i: ');
                    ReadLn(i);
                    Write('pavadinimas: ');
                    ReadLn(x.pavadinimas);
                    Write('mase: ');
                    ReadLn(x.mase);
                    Write('ar gyvenama(t/n): ');
                    ReadLn(ch);
                    x.ar_gyvenama:= ch = 't';
                    Write('id: ');
                    ReadLn(x.id);
                    Insert(L, x, i);
                except
                  on E: EInOutError do WriteLn('Blogai suvesti duomenys. ',
                  E.Message);
                end;
            end;
          '2':
            begin
                try
                    Write('i: ');
                    ReadLn(i);
                    Delete(L, i);
                except
                  on E: EInOutError do WriteLn('Blogai suvesti duomenys. ',
                  E.Message);
                end;
            end;
          '3':
            begin
                try
                    Write('i: ');
                    ReadLn(i);
                    p := Retrieve(L, i);
                    if p <> nil then
                       Isvesti(p^);
                except
                  on E: EInOutError do WriteLn('Blogai suvesti duomenys. ',
                  E.Message);
                end;
            end;
          '4':
            begin
              Kill(L);
              sukurta:=False;
            end;
          '5': Print(L);
          else
            begin
            Kill(L);
            exit;
            end;
          end;
      end;
  end;

end.

